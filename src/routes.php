<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    try{
        $search = $request->getQueryParam('search'); //

        if($search){
            $stmt = $this->db->prepare('SELECT * 
                                  FROM person 
                                  WHERE first_name ILIKE :s OR 
                                  last_name ILIKE :s OR 
                                  nickname ILIKE :s
                                  ORDER BY last_name');
            $stmt->bindValue(':s','%' . $search . '%');
            $stmt->execute();
        }else{
            $stmt = $this->db->query('SELECT * 
                                  FROM person 
                                  ORDER BY last_name');
        }

        $tplvars['persons'] = $stmt->fetchAll();
        return $this->view->render($response,'persons.latte',$tplvars);
    }
    catch (Exceptions $e){
        $this->logger->error($e->getMessage());
        die('something went wrong, try it again later')
;    }



})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/create-person', function (Request $request, Response $response, $args) {

    $tplVars['form']= [
        'fn'=> '',
        'ln'=> '',
        'nn'=> '',
        'bd'=> '',
        'l'=>'',
        'g'=>'male',
    ];

    return $this->view->render($response, 'create-person.latte', $tplVars);
})->setName('createPerson');

$app->post('/create-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if(!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn']) ){
        try{
            $stmt = $this->db->prepare('INSERT INTO person
                                    (first_name,last_name,nickname,birth_day,height,gender)
                                    VALUES (:fn,:ln,:nn,:bd,:l,:g)');

            $stmt->bindValue(':bd',empty($data['bd']) ? null : $data['bd']);
            $stmt->bindValue(':l',empty($data['l']) ? null : $data['l']);
            $stmt->bindValue(':g',empty($data['g']) ? null : $data['g']);
            $stmt->bindValue(':fn',$data['fn']);
            $stmt->bindValue(':ln',$data['ln']);
            $stmt->bindValue(':nn',$data['nn']);
            $stmt->execute();
           return $response->withHeader('Location',$this->router->pathFor('index'));
        }
        catch (Exception $e){
            if($e->getCode() == 23505){
                //duplicita
                $tplVars['error']='Tenot clovek uz existuje';
                $tplVars['form']=$data;
                return $this->view->render($response,
                    'create-person.latte',
                    $tplVars);

            }else{
                //neznama chyba
                $this->logger->error($e->getMessage());
                die('something went wrong, try it again later');
            }
            }

    } else {
            //vseobecna chyba
            $tplVars['error']='something went wrong, try it again later';
            $tplVars['form']=$data;
            return $this->view->render($response, 'create-person.latte', $tplVars);
    }
});